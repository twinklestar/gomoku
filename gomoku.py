from elements.game import Game


def main() -> None:
    game = Game()
    game.play()


main()
