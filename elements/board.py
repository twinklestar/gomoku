import re
import os
import copy
from typing import Any, Iterable, List, Tuple


SIZE = 15
P1_VICTORY_PATTERN = re.compile(r"11111")
P2_VICTORY_PATTERN = re.compile(r"22222")

PATTERN = [
    re.compile(r"211110|011112"),
    re.compile(r"011110"),
    re.compile(r"01110"),
    re.compile(r"2011100|0011102"),
    re.compile(r"010110|011010"),
    re.compile(r"0110|0110")
]

PATTERN_SCORE = [56, 37 * 56, 56, 56, 56, 1]

ADV_PATTERN = [
    re.compile(r"122220|022221"),
    re.compile(r"022220"),
    re.compile(r"02220"),
    re.compile(r"1022200|0022201"),
    re.compile(r"020220|022020"),
    re.compile(r"0220")
]


def spiral(n: int) -> List[Tuple[int, int]]:
    """
    Returns a list of coordinates to iterate a matrix of size n*n in spiral
    (outside in) order.
    """
    dx, dy = 1, 0  # Starting increments
    x, y = 0, 0    # Starting location
    matrix = [[-1]*n for _ in range(n)]
    for i in range(n**2):
        matrix[x][y] = i
        nx, ny = x + dx, y + dy
        if 0 <= nx < n and 0 <= ny < n and matrix[nx][ny] == -1:
            x, y = nx, ny
        else:
            dx, dy = -dy, dx
            x, y = x + dx, y + dy
    output = [(0, 0) for _ in range(n**2)]
    for i in range(n):
        for j in range(n):
            output[matrix[i][j]] = (i, j)
    return output


SPIRAL_ORDER = spiral(SIZE)[::-1]


def stringfy(matrix: List[List[int]]) -> str:
    string = ""
    for line in matrix:
        string += "".join(map(str, line)) + "\n"
    return string


class Board():
    """ A gomoku board, i.e., a state of the game. """

    def __init__(self) -> None:
        self._board = [[0 for _ in range(SIZE)] for _ in range(SIZE)]
        self._actual_player = 1
        self._last_play: Tuple[str, int] = ('', 0)

    def place_piece(self, position: Tuple[int, int]) -> None:
        x_coord, y_coord = position
        self._last_play = (chr(x_coord + 65), y_coord + 1)
        self._board[y_coord][x_coord] = self._actual_player
        self._actual_player = 1 if self._actual_player == 2 else 2

    def is_empty(self, position: Tuple[int, int]) -> bool:
        x_coord, y_coord = position
        return self._board[y_coord][x_coord] == 0

    @property
    def last_play(self) -> Tuple[str, int]:
        return self._last_play

    def _diagonals(self) -> List[List[int]]:
        return [[self._board[SIZE - p + q - 1][q]
                 for q in range(max(p - SIZE + 1, 0), min(p + 1, SIZE))]
                for p in range(SIZE + SIZE - 1)]

    def _antidiagonals(self) -> List[List[int]]:
        return [[self._board[p - q][q]
                 for q in range(max(p - SIZE + 1, 0), min(p + 1, SIZE))]
                for p in range(SIZE + SIZE - 1)]

    def _columns(self) -> List[List[int]]:
        return [[self._board[i][j]
                 for i in range(SIZE)]
                for j in range(SIZE)]

    def victory(self) -> int:
        """ player win: 1, computer win: 2, not end: 0"""
        whole_board = "\n".join(
            map(stringfy,
                [self._board,
                 self._diagonals(),
                 self._antidiagonals(),
                 self._columns()]))

        if P1_VICTORY_PATTERN.search(whole_board): return 1
        elif P2_VICTORY_PATTERN.search(whole_board): return 2
        else: return 0

    def evaluate(self) -> int:
        """ Returns an heuristic value of the current board. """
        whole_board = "\n".join(
            map(stringfy,
                [self._board,
                 self._diagonals(),
                 self._antidiagonals(),
                 self._columns()]))

        p1_value = 0
        p2_value = 0
        if P1_VICTORY_PATTERN.search(whole_board):
            p1_value += 2**25
        elif P2_VICTORY_PATTERN.search(whole_board):
            p2_value += 2**25

        for index, pattern in enumerate(PATTERN):
            p1_value += PATTERN_SCORE[index] * len(pattern.findall(whole_board))

        for index, pattern in enumerate(ADV_PATTERN):
            p2_value += PATTERN_SCORE[index] * len(pattern.findall(whole_board))

        return p2_value - p1_value

    def adjacents(self) -> Iterable[Any]:
        actual_board = copy.deepcopy(self)
        for i, j in SPIRAL_ORDER:
            if actual_board.is_empty((i, j)):
                actual_board.place_piece((i, j))
                yield actual_board
                actual_board._actual_player = \
                    1 if actual_board._actual_player == 2 else 2
                actual_board._board[j][i] = 0

    def get(self, y, x) -> int:
        return self._board[y][x]
