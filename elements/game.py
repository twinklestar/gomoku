import pygame, sys
from pygame.locals import *
from elements.board import Board
from ab_pruning import ab_pruning


class Game:

    background_color = (255, 255, 255)
    indian_red_1 = (255, 106, 106)
    cell = 40
    piece_width = 40
    piece_height = 40
    board_x = 2
    board_y = 2

    def __init__(self) -> None:
        self._board = Board()
        self.turn = 0  # it's player's turn to play, and it's computer's turn to play when `turn` equals 1.

        # load pictures
        self.board_background = pygame.image.load('elements/pic/board.jpg')
        self.board_rect = self.board_background.get_rect()

        self.black_piece = pygame.image.load('elements/pic/black_piece.jpg')
        self.black_rect = self.black_piece.get_rect()

        self.white_piece = pygame.image.load('elements/pic/white_piece.jpg')
        self.white_rect = self.white_piece.get_rect()

        # set window
        self.window_surface = pygame.display.set_mode((self.board_rect.width, self.board_rect.height))
        pygame.display.set_caption('Gobang')

    def play(self) -> None:

        # initialization
        pygame.init()
        clock = pygame.time.Clock()
        font = pygame.font.SysFont('Consolas', 50)

        # main circulation
        while not self._board.victory():

            self.window_surface.fill(self.background_color)
            self.window_surface.blit(self.board_background, self.board_rect, self.board_rect)

            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                if self.turn == 0 and event.type == MOUSEBUTTONDOWN and event.button == 1:
                    x, y = pygame.mouse.get_pos()
                    col = int((x - self.board_x) / self.cell)
                    row = int((y - self.board_y) / self.cell)
                    if self._board.get(col, row) == 0:  # can't place on other chess pieces
                        pos = (row, col)
                        self._board.place_piece(pos)
                        self.turn = 1

            self.display_board()

            if self.turn == 1:
                self._board, _ = \
                    ab_pruning(self._board, 2, -2 ** 32, 2 ** 32, True)
                self.turn = 0

            self.display_board()

            pygame.display.update()
            clock.tick(40)

        if self._board.victory() == 1:  # player win
            result = 'Congratulations! You win.'
        else:  # computer win
            result = 'What a pity! You lose.'

        text = font.render(result, True, self.indian_red_1)
        text_rect = text.get_rect()
        text_rect.centerx = self.window_surface.get_rect().centerx
        text_rect.centery = self.window_surface.get_rect().centery
        self.window_surface.blit(text, text_rect)
        pygame.display.update()

    def display_board(self) -> None:
        for x in range(15):
            for y in range(15):
                rect_dst = pygame.Rect(self.board_x + x * self.cell, self.board_y + y * self.cell, self.piece_width,
                                      self.piece_height)
                if self._board.get(x, y) == 1:
                    self.window_surface.blit(self.black_piece, rect_dst, self.black_rect)
                elif self._board.get(x, y) == 2:
                    self.window_surface.blit(self.white_piece, rect_dst, self.white_rect)
